export const state = () => ({
  drawer: false
})

export const mutations = {
  toggleDrawer(state, payload) {
    state.drawer = payload
  }
}

export const getters = {
  getDrawer(state) {
    return state.drawer
  }
}



