const axios = require("axios")


//State//
export const state = () => ({
  // categories
  categories: [],
  slugCat: [],
  subcategories:[],
  slugSubcat:[],
  postPage:[]
})




//Mutations //

export const mutations = {

  //set categories
  setCategories(state, categories) {
    state.categories = categories
  },
  //set slugs of categories
  setslugCat(state, slugCat) {
    state.slugCat = slugCat
  },

  //set subcategories
  setsubCategories(state, subcategories) {
    state.subcategories = subcategories
  },
  //set slugs of subcategories
  setslugSubCat(state, slugSubcat) {
    state.slugsubCat = slugSubcat
  },

  //set postpage
  setpostPage(state, postPage) {
    state.postPage = postPage
  }


}


//Actions //

export const actions = {

  async nuxtServerInit({commit}) {


    //set categories & SlugCat
    const response = await axios.get('http://www.mocky.io/v2/5ec604783200007000d749af')
    const data = response.data
    const slugs = data.map(({slug}) => slug)

    commit("setCategories", data)
    commit("setslugCat",  slugs)


    //set subCategories & SlugSubcat
    const response1 = await axios.get('http://www.mocky.io/v2/5ec6ce983200005e00d7516a')
    const data1 = response.data
    const slugs1 = data.map(({slug}) => slug)

    commit("setsubCategories", data1)
    commit("setslugSubCat",  slugs1)


    //set postPage
    const response2 = await axios.get('http://www.mocky.io/v2/5ec635553200007900d74b52')
    const data2 = response2.data
    commit("setpostPage", data2)






  }


}
