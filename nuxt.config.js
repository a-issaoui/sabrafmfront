import colors from 'vuetify/es5/util/colors'


export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [
    '~/assets/css/m3soft.scss',
    '~/assets/css/animate.min.css',
    '~/assets/scss/correction.sass',
    '~/assets/css/font-icons.css',
    '~/assets/css/hover.scss'
  ],
  /*
  ** Fonts loaded by webfonts
  */


debug: true,




  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {src: "~/plugins/vClickOutside", ssr: false},
    {src: "~/plugins/helpers"},
    {src: "~/plugins/MarqueeText", ssr: false},
    {src: "~/plugins/v-style", ssr: false},
    {src: "~/plugins/aos", ssr: false},
    {src: "~/plugins/vue-carousel", ssr: false},
    {src: "~/plugins/vue-carousel-card"},
    {src: "~/plugins/VueSpinners"},
    {src: "~/plugins/fontawesome"},
    {src: "~/plugins/vue-social-sharing", ssr: false},
    {src: "~/plugins/vue-audio-better", ssr: false},
    {src: "~/plugins/vue-easy-lightbox", ssr: false},
    {src: "~/plugins/vue-cool-lightbox", ssr: false},
    {src: "~/plugins/paginate", ssr: false}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
    ['@nuxtjs/moment', {
      locales: ['ar-tn']
    }]
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {},
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/scss/variables.scss'],
    rtl: true,
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, {isDev, isClient}) {
      config.resolve.alias.vue = 'vue/dist/vue.common';
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          exclude: /(node_modules)/
        })
      }
    },
    cssSourceMap: false
  },
  /********Rendering**********/

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        return ['script', 'style', 'font'].includes(type)
      }
    }
  }
}
