import Vue from 'vue'

Vue.mixin({
  methods: {
    mySpecialMethod(value) {
      console.log(value)
    },
    getYoutubeThumb(url, size) {
        if (url === null) {
          return '';
        }
        size    = (size === null) ? 'big' : size
        var results = url.match('[\\?&]v=([^&#]*)')
        var video   = (results === null) ? url : results[1]

        if (size === 'small') {
          return 'http://img.youtube.com/vi/' + video + '/mqdefault.jpg'
        }
        return 'http://img.youtube.com/vi/' + video + '/maxresdefault.jpg'
      },
    htmltotxt(a){
      var d = document.createElement('div');
      d.innerHTML = a;
      return d.innerText;
    },
    truncate(value, limit) {
      if (value.length > limit) {
        value = value.substring(0, (limit - 3)) + '...';
      }
      return value ;
    }
    }
})



